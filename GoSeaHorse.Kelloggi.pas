unit GoSeaHorse.Kelloggi;

{$Q-,R-,B-,X+,T-}

interface

  uses
    System.Generics.Defaults;

  type
    IEnumerator<T> = interface
    ['{24A44A8F-F6F6-469E-BFB5-E42AA107AAC7}']
      function MoveNext: Boolean;
      {$REGION 'Property accessors'}
      function GetCurrent: T;
      {$ENDREGION}

      property Current: T read GetCurrent;
    end;

    IEnumerable<T> = interface
    ['{14969B61-AACD-4D63-AF54-A7230CF0821B}']
      function GetEnumerator: IEnumerator<T>;
    end;

    TArray = record
    public
      class procedure Copy<T>(const Source: TArray<T>; var Destination: TArray<T>; Count: NativeInt); overload; static;
      class procedure Copy<T>(const Source: TArray<T>; SourceIndex: NativeInt; var Destination: TArray<T>; DestinationIndex: NativeInt; Count: NativeInt); overload; static;
    end;

    TQueue<T> = class(TInterfacedObject, IEnumerable<T>)
    strict private
      FCapacity: Int32;
      FComparer: IEqualityComparer<T>;
      FCount: Int32;
      FHead: Int32;
      FItems: TArray<T>;
      FTail: Int32;
      FVersion: Int32;

      procedure EnsureCapacity(Capacity: Int32);

      const
        MinimumCapacity: Int32 = 4;

      type
        TEnumerator = class(TInterfacedObject, IEnumerator<T>)
        strict private
          FCurrent: T;
          FIndex: Int32;
          FQueue: TQueue<T>;
          FVersion: Int32;

          function MoveNext: Boolean;
          {$REGION 'Property accessors'}
          function GetCurrent: T;
          {$ENDREGION}

          property Current: T read GetCurrent;
        private
          constructor Create(const Queue: TQueue<T>);
        end;
    public
      constructor Create; overload;
      constructor Create(Capacity: Int32); overload;
      constructor Create(const Collection: IEnumerable<T>); overload;
      procedure Clear;
      function Contains(const Item: T): Boolean;
      function Dequeue: T;
      procedure Enqueue(const Item: T);
      function GetEnumerator: IEnumerator<T>;
      function Peek: T;
      procedure TrimExcess;

      property Count: Int32 read FCount;
    end;

    TKeyValuePair<TKey, TValue> = record
    private
      FKey: TKey;
      FValue: TValue;
    public
      constructor Create(const Key: TKey; const Value: TValue);

      property Key: TKey read FKey;
      property Value: TValue read FValue;
    end;

implementation

  uses
    GoSeaHorse.Kelloggi.Constants, System.SysUtils;

  type
    TInt32Helper = record helper for Int32
    private
      function NextPowerOfTwo: Int32; inline;
    end;

  {$REGION 'TInt32Helper'}
  function TInt32Helper.NextPowerOfTwo: Int32;
  var
    Rounded: Int32;
  begin
    Rounded := Self - 1;
    Rounded := Rounded or (Rounded shr 1);
    Rounded := Rounded or (Rounded shr 2);
    Rounded := Rounded or (Rounded shr 4);
    Rounded := Rounded or (Rounded shr 8);
    Rounded := Rounded or (Rounded shr 16);

    Result := Rounded + 1;
  end;
  {$ENDREGION}

  {$REGION 'TArray'}
  class procedure TArray.Copy<T>(const Source: TArray<T>; var Destination: TArray<T>; Count: NativeInt);
  begin
    Copy<T>(Source, 0, Destination, 0, Count);
  end;

  class procedure TArray.Copy<T>(const Source: TArray<T>; SourceIndex: NativeInt; var Destination: TArray<T>; DestinationIndex: NativeInt; Count: NativeInt);
  begin
    if IsManagedType(T) then
      CopyArray(@Destination[DestinationIndex], @Source[SourceIndex], TypeInfo(T), Count)
    else
      Move(Source[SourceIndex], Destination[DestinationIndex], Count * SizeOf(T));
  end;
  {$ENDREGION}

  {$REGION 'TQueue<T>'}
  constructor TQueue<T>.Create;
  begin
    Create(MinimumCapacity);
  end;

  constructor TQueue<T>.Create(Capacity: Int32);
  begin
    if Capacity < 0 then
      raise EArgumentOutOfRangeException.CreateRes(@SArgumentOutOfRange_NegativeNumber_Capacity);

    if Capacity < MinimumCapacity then
      FCapacity := MinimumCapacity
    else
      FCapacity := Capacity.NextPowerOfTwo;

    SetLength(FItems, FCapacity);

    FComparer := TEqualityComparer<T>.Default;
  end;

  constructor TQueue<T>.Create(const Collection: IEnumerable<T>);
  var
    LItem: T;
  begin
    Create(MinimumCapacity);

    if Collection = nil then
      raise EArgumentNilException.CreateRes(@SArgumentNil_NilValue_Collection);

    for LItem in Collection do
      Enqueue(LItem);
  end;

  procedure TQueue<T>.Clear;
  var
    LIndex: Int32;
    LMask: Int32;
  begin
    if FCount > 0 then
    begin
      LIndex := FHead;
      LMask := Length(FItems) - 1;

      repeat
        FItems[LIndex] := Default(T);

        LIndex := (LIndex + 1) and LMask;
      until LIndex = FTail;

      FHead := 0;
      FTail := 0;
      FCount := 0;
      FVersion := FVersion + 1;
    end;
  end;

  function TQueue<T>.Contains(const Item: T): Boolean;
  var
    LCount: Int32;
    LIndex: Int32;
    LMask: Int32;
  begin
    LIndex := FHead;
    LMask := Length(FItems) - 1;
    LCount := FCount;

    Result := False;

    while (LCount > 0) and (not Result) do
    begin
      if FComparer.Equals(FItems[LIndex], Item) then
      begin
        Result := True;  
      end else
      begin
        LIndex := (LIndex + 1) and LMask;
        LCount := LCount - 1;
      end;
    end;
  end;

  function TQueue<T>.Dequeue: T;
  begin
    if Count = 0 then
      raise EInvalidOpException.CreateRes(@SInvalidOp_EmptyQueue);

    Result := FItems[FHead];

    FItems[FHead] := Default(T);

    FHead := (FHead + 1) and (Length(FItems) - 1);
    FCount := FCount - 1;
    FVersion := FVersion + 1;
  end;

  procedure TQueue<T>.EnsureCapacity(Capacity: Int32);
  var
    LItems: TArray<T>;
    LRight: Int32;
  begin
    SetLength(LItems, Capacity);

    if FHead < FTail then
    begin
      TArray.Copy<T>(FItems, FHead, LItems, 0, Count);
    end else 
    begin
      LRight := Length(FItems) - FHead;

      TArray.Copy<T>(FItems, FHead, LItems, 0, LRight);
      TArray.Copy<T>(FItems, 0, LItems, LRight, FTail);
    end;

    FItems := LItems;

    FHead := 0;
    FTail := Count;
  end;

  procedure TQueue<T>.Enqueue(const Item: T);
  begin
    if Count = Length(FItems) then
      EnsureCapacity(Count shl 1);

    FItems[FTail] := Item;

    FTail := (FTail + 1) and (Length(FItems) - 1);
    FCount := FCount + 1;
    FVersion := FVersion + 1;
  end;

  function TQueue<T>.GetEnumerator: IEnumerator<T>;
  begin
    Result := TEnumerator.Create(Self);
  end;

  function TQueue<T>.Peek: T;
  begin
    if Count = 0 then
      raise EInvalidOpException.CreateRes(@SInvalidOp_EmptyQueue);

    Result := FItems[FHead];
  end;

  procedure TQueue<T>.TrimExcess;
  begin
    EnsureCapacity(Count);

    FVersion := FVersion + 1;
  end;
  {$ENDREGION}

  {$REGION 'TKeyValuePair<TKey, TValue>'}
  constructor TKeyValuePair<TKey, TValue>.Create(const Key: TKey; const Value: TValue);
  begin
    FKey := Key;

    FValue := Value;
  end;
  {$ENDREGION}

  {$REGION 'TQueue<T>.TEnumerator'}
  constructor TQueue<T>.TEnumerator.Create(const Queue: TQueue<T>);
  begin
    FQueue := Queue;

    FVersion := FQueue.FVersion;
    FIndex := -1;
  end;

  function TQueue<T>.TEnumerator.GetCurrent: T;
  begin
    if FIndex < 0 then
      EInvalidOpException.CreateRes(@SInvalidOp_NotStartedEnumerator);

    if FIndex = FQueue.FCount then
      EInvalidOpException.CreateRes(@SInvalidOp_CompletedEnumerator);
      
    Result := FCurrent;
  end;

  function TQueue<T>.TEnumerator.MoveNext: Boolean;
  begin
    if FVersion <> FQueue.FVersion then
      EInvalidOpException.CreateRes(@SInvalidOp_OutdatedEnumerator);

    FIndex := FIndex + 1;

    if FIndex < FQueue.FCount then
    begin
      FCurrent := FQueue.FItems[(FQueue.FHead + FIndex) and (Length(FQueue.FItems) - 1)];

      Result := True;    
    end
    else begin
      FCurrent := Default(T);
    
      Result := False;
    end;
  end;
  {$ENDREGION}

end.
