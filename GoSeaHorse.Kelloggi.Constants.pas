unit GoSeaHorse.Kelloggi.Constants;

interface

  resourcestring
    SArgumentNil_NilValue_Collection = 'Value cannot be nil.' + SLineBreak + 'Parameter name: Collection';
    SArgumentOutOfRange_NegativeNumber_Capacity = 'Non-negative number required.' + SLineBreak + 'Parameter name: Capacity';
    SInvalidOp_EmptyQueue = 'Empty queue';
    SInvalidOp_NotStartedEnumerator = 'Enumerator has not started. Call MoveNext';
    SInvalidOp_CompletedEnumerator = 'Enumerator has already completed';
    SInvalidOp_OutdatedEnumerator = 'Collection was modified after the enumerator was instantiated';

implementation

end.
